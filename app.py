import os
import uuid
from flask import Flask
from flask import request
from flask import jsonify, send_file
from flask_cors import CORS

from openpose_wrapper import OpenPose
from openpose_wrapper_old import OpenPose as OpenPoseOld


app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = '/tmp'

open_pose_old = OpenPoseOld('/tmp')
open_pose = OpenPose('/tmp')

def save_image_from_request(request):
    file = request.files['file']
    extension = os.path.splitext(file.filename)[1]
    f_name = str(uuid.uuid4()) + extension
    full_path = os.path.join(app.config['UPLOAD_FOLDER'], f_name)
    file.save(full_path)
    return full_path

@app.route('/get_body_image_json', methods=['POST'])
def get_body_image_json_old():
    json_data, image_result_path = open_pose_old.get_json_body(save_image_from_request(request))
    return jsonify(json_data)

@app.route('/get_face_image_json', methods=['POST'])
def get_hand_image_json_old():
    json_data, image_result_path = open_pose_old.get_json_face(save_image_from_request(request))
    return jsonify(json_data)

@app.route('/get_hand_image_json', methods=['POST'])
def get_face_image_json_old():
    json_data, image_result_path = open_pose_old.get_json_hand(save_image_from_request(request))
    return jsonify(json_data)


@app.route('/get_json/<model>', methods=['POST'])
def get_json(model):
    json_data, _ = open_pose.get_result(save_image_from_request(request), model)
    return jsonify(json_data)

@app.route('/get_image/<model>', methods=['POST'])
def get_image(model):
    _, image_path = open_pose.get_result(save_image_from_request(request), model, save_image=True)
    return send_file(image_path)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

