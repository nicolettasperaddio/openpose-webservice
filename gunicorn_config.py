import os

PORT = os.getenv('PORT')
WORKERS = os.getenv('WORKERS')

bind = '0.0.0.0:{}'.format(PORT)
workers = WORKERS
timeout = 360

errorlog = '-'
loglevel = 'debug'
accesslog = '-'

pythonpath = os.getenv('PYTHONPATH')

