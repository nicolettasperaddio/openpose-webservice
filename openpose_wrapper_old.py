import os
import sys
import uuid
import json
from pprint import pprint
import subprocess

class OpenPose(object):
    def __init__(self, base_dir):
        self.base_dir = base_dir

    def get_result(self, image_path, command):
        dir_name = str(uuid.uuid4())
        full_dir_name = os.path.join(self.base_dir, dir_name)

        os.mkdir(full_dir_name)
        image_dir = os.path.join(full_dir_name, 'origin_image')
        result_dir = os.path.join(full_dir_name, 'result_image')
        result_json_dir = os.path.join(full_dir_name, 'result_json')

        os.makedirs(image_dir)
        os.makedirs(result_dir)
        os.makedirs(result_json_dir)

        image_name = os.path.basename(image_path)
        image_result_path = os.path.join(result_json_dir, image_name.split('.')[0] + '_rendered.png')
        json_result_path = os.path.join(result_json_dir, image_name.split('.')[0] + '_keypoints.json')
        os.symlink(image_path, os.path.join(image_dir, image_name))

        command = command.format(image_dir=image_dir,
                       result_dir=result_dir,
                       result_json_dir=result_json_dir)
        print(command)
        subprocess.call(command, shell=True)
        with open(json_result_path, 'r') as f:
            json_data = json.load(f)

            for p in json_data['people']:
                p['pose_keypoints'] = p['pose_keypoints_2d']
                p['face_keypoints'] = p['face_keypoints_2d']
                p['hand_left_keypoints'] = p['hand_left_keypoints_2d']
                p['hand_right_keypoints'] = p['hand_right_keypoints_2d']
        return json_data, image_result_path

    def get_json_body(self, image_path):
        command = ("/app/openpose/build/examples/openpose/openpose.bin "
                   "--display 0 "
                   "--image_dir {image_dir} "
                   "--write_images {result_dir} "
                   "--write_json {result_json_dir}")
        return self.get_result(image_path, command)

    def get_json_face(self, image_path):
        command = ("/app/openpose/build/examples/openpose/openpose.bin "
                   "--display 0 --face "
                   "--image_dir {image_dir} "
                   "--write_images {result_dir} "
                   "--write_json {result_json_dir}")
        return self.get_result(image_path, command)

    def get_json_hand(self, image_path):
        command = ("/app/openpose/build/examples/openpose/openpose.bin "
                   "--display 0 --hand "
                   "--image_dir {image_dir} "
                   "--write_images {result_dir} "
                   "--write_json {result_json_dir}")
        return self.get_result(image_path, command)
