import os
import sys
import uuid
import json
from pprint import pprint
import subprocess
import cv2

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(dir_path, './build/python'))
import openpose

class OpenPose(object):
    def __init__(self, base_dir):
        self.base_dir = base_dir
        self.models = {}

    def init_model_ifnotyet(self, model_pose):
        if model_pose not in self.models:
            params = {}
            params["logging_level"] = 3
            params["output_resolution"] = "-1x-1"
            params["net_resolution"] = "-1x368"

            # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/src/openpose/utilities/flagsToOpenPose.cpp
            params["model_pose"] = model_pose

            params["alpha_pose"] = 0.6
            params["scale_gap"] = 0.3
            params["scale_number"] = 1
            params["render_threshold"] = 0.05
            # If GPU version is built, and multiple GPUs are available, set the ID here
            params["num_gpu_start"] = 0
            params["disable_blending"] = False
            params["default_model_folder"] = os.path.join(dir_path, "./models/")

            self.models[model_pose] = openpose.OpenPose(params)

        return self.models[model_pose]

    def create_people_obj(self):
        return {
            'face_keypoints': [],
            'face_keypoints_3d': [],
            "hand_left_keypoints": [],
            "hand_left_keypoints_3d": [],
            "hand_right_keypoints": [],
            "hand_right_keypoints_3d": [],
            "pose_keypoints": [],
            "pose_keypoints_3d": []
        }

    def get_result(self, image_path, model, save_image=False):
        img = cv2.imread(image_path)
        keypoints = None
        output_image = None
        if save_image:
            keypoints, output_image = self.init_model_ifnotyet(model).forward(img, save_image)
        else:
            keypoints = self.init_model_ifnotyet(model).forward(img, save_image)

        outimage_path = None
        if save_image:
            outimage_path = os.path.join('/tmp', str(uuid.uuid4()) + '.jpg')
            cv2.imwrite(outimage_path, output_image)

        result = {
            'people': []
        }

        people_kp_keys = {
            'BODY_25': 'pose_keypoints',
            'COCO': 'pose_keypoints'
        }
        if model not in people_kp_keys:
            print('Unknown model', model)
            people_kp_keys[model] = model + '_keypoints'
        people_key = people_kp_keys[model]

        for k in keypoints:
            p = self.create_people_obj()
            p[people_key] = []
            for kp in k:
                p[people_key].extend(kp.tolist())
            result['people'].append(p)
        return result, outimage_path
