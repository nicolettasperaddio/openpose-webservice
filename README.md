# Openpose Web-service wrapper

## Usage

### Run

```bash
nvidia-docker run -ti -p 5002:5000 -e PORT=5000 -e WORKERS=1 -e CUDA_VISIBLE_DEVICES=0 neuromationorg/openpose-webservice
```

### Calculate keypoints

#### Old style (by `openpose.bin`)

This returns JSON with requested markup.

```bash
curl -F 'file=@image.jpg' http://192.168.2.2:5102/get_body_image_json
curl -F 'file=@image.jpg' http://192.168.2.2:5102/get_face_image_json
curl -F 'file=@image.jpg' http://192.168.2.2:5102/get_hand_image_json
```

#### New style

Without shutting down and relaunching.

```bash
curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_json/<MODEL>
curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_image/<MODEL>
```

Examples:

```bash
curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_json/BODY_25
curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_json/COCO

curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_image/BODY_25 > b.jpg
curl -F 'file=@fhOBpaP.jpg' http://192.168.2.2:5102/get_image/COCO > b.jpg
```
